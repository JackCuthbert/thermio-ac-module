# thermio [![Build Status](https://travis-ci.org/JackCuthbert/thermio.svg?branch=master)](https://travis-ci.org/JackCuthbert/thermio)

Use arduino to control your air conditioner and supply room condition information.

### Read climate data

The arduino publishes the following to MQTT queue with topic `climate`, at a specified interval (env.h).

```json
{
  "temperature": 23.40,
  "humidity": 44.20,
  "heatIndex": 22.95
}
```

### Get aircon settings

A two step process.

Publish the following to MQTT queue with topic `heatpump`.

```json
{
  "type": "get"
}
```

The Arduino will then publish the following to MQTT queue with topic `settings` after it has finished processing the previous message.

```json
{
  "power": 1,
  "mode": 1,
  "fan": 0,
  "temp": 24,
  "vDir": 0,
  "hDir": 0
}
```

### Set aircon settings

Publish the following to MQTT queue with topic `heatpump`. Arduino will then process this and send the correct IR signal to the aircon.

```json
{
  "type": "set",
  "power": 1,
  "mode": 1,
  "fan": 0,
  "temp": 24,
  "vDir": 0,
  "hDir": 0
}
```

The Arduino will then publish the parsed settings in the same format as the `"type": "get"` response.