/************************** Environment ***************************/
#include "env.h"



/************************** WiFi Setup ****************************/
#include <ESP8266WiFi.h>
WiFiClient espClient;



/************************** MQTT/JSON Setup ***********************/
#include <ArduinoJson.h>
#include <PubSubClient.h>
PubSubClient client(espClient);

IRSenderBitBang irSender(IRPIN);

struct Heatpump {
  byte power;
  byte mode;
  byte fan;
  byte temp;
  byte vDir;
  byte hDir;
};

Heatpump heatpump = {
  POWER_OFF,
  MODE_AUTO,
  FAN_AUTO,
  24,
  VDIR_AUTO,
  HDIR_AUTO
};

/************************** Setup! **************************/

void setup_wifi() {
  delay(500);

  Serial.print("Attempting WiFi connection");

  delay(10);

  // Disable access point mode
  WiFi.mode(WIFI_STA);

  // Begin connecting to wifi
  if (WiFi.SSID() != WIFI_SSID) {
    WiFi.begin(WIFI_SSID, WIFI_PASS);
    WiFi.persistent(true);
    WiFi.setAutoConnect(true);
    WiFi.setAutoReconnect(true);
  }

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println(" Connected.");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
}

void setup_pubsub() {
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(handle_mqtt);
  reconnect_pubsub();
}

void reconnect_pubsub() {
  while(!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect(WiFi.macAddress().c_str())) {
      Serial.println(" Connected.");
      client.publish("connection", "Connected.");
      client.subscribe("heatpump");
    } else {
      Serial.print("Connection failed, error code: ");
      Serial.print(client.state());
      Serial.println(". Will retry in 5 seconds.");

      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(9600);
  setup_wifi();
  setup_pubsub();
  setup_heatpump();

  delay(1000);
  Serial.println("Starting...");
}

/************************** Loop! **************************/
long lastPublishTime = 0;

void loop() {
  // Maintain MQTT connection
  if (!client.connected()) {
    reconnect_pubsub();
  }

  // Magic event listener loop
  client.loop();

  delay(100);
}

void handle_mqtt(char* topic, byte* payload, unsigned int length) {
  payload[length] = '\0';
  DynamicJsonBuffer jsonBuffer;
  JsonObject& payloadData = jsonBuffer.parseObject(payload);

  mergeHeatpumpData(payloadData);

  if (payloadData.containsKey("type") && strcmp(payloadData["type"].as<char*>(), "set") == 0) {
    heatpumpIR->send(
      irSender,
      heatpump.power,
      heatpump.mode,
      heatpump.fan,
      heatpump.temp,
      heatpump.vDir,
      heatpump.hDir
    );
  }

  JsonObject& heatpumpData = jsonBuffer.createObject();

  heatpumpData["power"] = heatpump.power;
  heatpumpData["mode"]  = heatpump.mode;
  heatpumpData["fan"]   = heatpump.fan;
  heatpumpData["temp"]  = heatpump.temp;
  heatpumpData["vDir"]  = heatpump.vDir;
  heatpumpData["hDir"]  = heatpump.hDir;

  char encodedJson[180];
  heatpumpData.printTo(encodedJson, sizeof(encodedJson));
  client.publish("settings", encodedJson);
}

// Replace heatpump state with any new settings
void mergeHeatpumpData(JsonObject& src) {
  if (src.containsKey("power")) {
    heatpump.power = src["power"];
  }

  if (src.containsKey("mode")) {
    heatpump.mode = src["mode"];
  }

  if (src.containsKey("fan")) {
    heatpump.fan = src["fan"];
  }

  if (src.containsKey("temp")) {
    heatpump.temp = src["temp"];
  }

  if (src.containsKey("vDir")) {
    heatpump.vDir = src["vDir"];
  }

  if (src.containsKey("hDir")) {
    heatpump.hDir = src["hDir"];
  }
}
